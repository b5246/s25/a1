// DATABASE
use fruits;




//#2 Use the count operator to count the total number of fruits on sale.

// OPTION1 : 3 == total per fruit name - onsale
db.fruits.find({onSale: true}).count(); 

//OR
db.fruits.aggregate( [
	{$match: {onSale: true}},
	{ $group: { _id: null, myCount: { $sum: 1 } } },
	{ $project: { _id: 0 } }
] );   

// OR 
db.fruits.aggregate( [
    {$match: {onSale: true}},
	{$count: "total_number_of_fruits_on_sale"}
] ); 


 // OPTION 2: "myCount" : 60.0 == Total per stock -on sale
db.fruits.aggregate( [
	{$match: {onSale: true}},
	{ $group: { _id: null, myCount: { $sum: "$stock" } } },
	{ $project: { _id: 0 } }
] );    



//#3 Use the count operator to count the total number of fruits with stock more than 20.
// OPTION 1: 1 == Sorted by fruit name
db.fruits.find({stock:{$gt:20}}).count();  

//OR
db.fruits.aggregate( [
        {$match: {stock:{$gt:20}}},
	{ $group: { _id: null, myCount: { $sum: 1 } } },
	{ $project: { _id: 0 } }
] );   

// OR
db.fruits.aggregate( [
    {$match: {stock:{$gt:20}}},
	{ $count: "total_number_of_fruits_(>20)"}
] ); 


// OPTION 2: "myCount=25" == SOrted and overall total of the  Stock
db.fruits.aggregate( [
        {$match: {stock:{$gt:20}}},
	{ $group: { _id: null, myCount: { $sum: "$stock" } } },
	{ $project: { _id: 0 } }
] );  

// #4  Use the average operator to get the average price of fruits onSale per supplier.
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group:{ 
			_id: "$supplier_id",
			averagePrice: {$avg:"$price"}
		}
	}
]);

// #5 Use the max operator to get the highest price of a fruit per supplier.
db.fruits.aggregate([
    {$group:{ 
			_id: "$supplier_id",
			maximumPrice: {$max:"$price"}
		}
	}
]);

// #6 Use the min operator to get the lowest price of a fruit per supplier.
db.fruits.aggregate([
    {$group:{ 
			_id: "$supplier_id",
			minimumPrice: {$min:"$price"}
		}
	}
]);

